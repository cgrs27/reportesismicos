from tkinter import *
from backendReporte import GenReporte
window = Tk()
a = GenReporte()
evento = ""
tabla = []
lista = []
def callback():
    pass
def delete_command():
    print(a.lista[-1])
    # for e in seleccion:
    #     list.delete(e+1)
    #     a.delete(e)

# def get_selected_row(event):
#     evento = event
#     index = list.curselection()
#     #print(index)
#     return(index)
def get_selected_set(event):
    evento = event
    a.lista = list.curselection()
    #return lista
def reporteSismos():
    if a.getDatos() != None and a.getDatos() != []:
        a.entablar()
        a.go()
    else:
        print("no hay datos")

def cargarLista():

    if a.getFecha() == ''or a.getFecha != fecha_text:
        list.delete(0,END)
        a.setFecha(fecha_text)
        # print(a.getFecha())
    a.populate()

    for row in a.getDatos():
        elem= ''
        for e in row:
            elem+=e+'  '

        list.insert(END,elem)


ltitle = Label(window,text='GenReport')
ltitle.grid(row=0,column=3)

lfecha = Label(window,text='Fecha')
lfecha.grid(row=2,column=0)

lgrs = Label(window,text='Generar reporte de sismos')
lgrs.grid(row=2,column=3)


lgrss = Label(window,text='Generar reporte de sismo sentido')
lgrss.grid(row=3,column=3)

lcredito = Label(window,text='Creado por Carlos Ramirez')
lcredito.grid(row=17,column=2)

fecha_text = StringVar()
efecha = Entry(window,textvariable=fecha_text)
efecha.grid(row=2,column=1)

bsismos = Button(window, text="Enviar", width=12, command=reporteSismos)
bsismos.grid(row=2,column=7)

bsentido = Button(window, text="Enviar", width=12, command=callback)
bsentido.grid(row=3,column=7)

bclose = Button(window, text="Close", width=12, command=callback)
bclose.grid(row=4,column=7)

bfecha = Button(window, text="Mostrar", width=12, command=cargarLista)
bfecha.grid(row=3,column=1)

bdelete = Button(window, text="Borrar Seleccion", width=12, command=delete_command)
bdelete.grid(row=8,column=7)

list=Listbox(window,selectmode=EXTENDED,height=10,width=130)
list.grid(row=6,column=0,rowspan=6,columnspan=6)
list.bind('<<ListboxSelect>>',get_selected_set)#get_selected_row,get_selected_set)

sb = Scrollbar(window)
sb.grid(row=8,column=6,rowspan=8)

list.configure(yscrollcommand=sb.set)
sb.configure(command=list.yview)

window.mainloop()

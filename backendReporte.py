# Programa generador del reporte de sismos del dia
# Generador de Reportes v1.0
# creado por Carlos Ramirez

from reportlab.platypus import (SimpleDocTemplate, Paragraph, Spacer,Table,
TableStyle)
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.rl_config import defaultPageSize
from reportlab.lib.units import inch
from reportlab.lib.pagesizes import letter,landscape
from reportlab.pdfgen import canvas
from reportlab.lib import colors
import os
# C:\Users\analista 1\Desktop\Boletines2020\Bfebrero\Boletin2020-02-07.pdf


class GenReporte:
    ''' la clase GenReport contiene todo los metodos y variables necesarias para
        generar un reporte de sismos con los datos del catalogo del
        sismologico'''

    def __init__(self):
        self.PAGE_HEIGHT=defaultPageSize[0]; self.PAGE_WIDTH=defaultPageSize[1]
        self.path_archivo =open("paths.txt").readlines()[0][:-1]#direccion del archivo dummyX.copy
        self.path_boletin = open("paths.txt").readlines()[1][:-1]
        self.name = open("paths.txt").readlines()[1][:-1]
        self.logos="logos_reporte.png"
        self.pie_con_firma='pie_con_firma.png'
        self.pie_pagina1 = 'El tiempo está dado en hora internacional, para obtener la hora local reste 4 horas. (Ej. 1400 es 10:00 AM).'
        self.pie_pagina2 = 'La magnitud está dada en grados en la escala de Richter.'
        self.cabecera2 = ['aaaa-mm-dd','hh:mm:ss','lat N','Lon W','(Km)','G']
        self.cabecera1=['Fecha','Tiempo','Coordenadas','','Prof','Mag','Comentarios']
        # self.fecha = ''
        self.sentidos = []
        self.fechaInicial = ''
        self.fechaFinal = ''
        self.maxx = 15
        self.datos=[]
        self.tablas=[]
        self.lista = []

    def reset(self):
        self.sentidos = []
        self.fechaInicial = ''
        self.fechaFinal = ''
        self.datos=[]
        self.tablas=[]
        self.lista = []
        #self.cargar()

    def set_sentidos(self,lista):
        '''funcion que recibe la lista de los seleccionados para
            mandarlos como sismos sentidos'''
        for i in lista:
            self.sentidos.append(self.getDatos()[i])

    def cargar(self):
        lista = open(self.path_archivo).readlines()
        for n in lista:
            if 'ï¿½' in n:
                n = n.replace('ï¿½','ñ')
            self.datos.append(n[:-1])

    def crearNombrepdf(self):
        ruta = open("paths.txt").readlines()[1][:-1]
        nombre_boletin = f'\Boletin{self.fechaInicial[:4]}-{self.fechaInicial[4:6]}-{self.fechaInicial[6:8]}.pdf'
        # print(ruta)
        self.name =os.path.abspath(ruta+nombre_boletin)
        print(self.name)

    def getFechaInicial(self):
        return self.fechaInicial

    def setFechaInicial(self,fechaInicial):
        self.fechaInicial = fechaInicial

    def getFechaFinal(self):
        return self.fechaFinal

    def setFechaFinal(self,fechaFinal):
        self.fechaFinal = fechaFinal

    def getTablas(self):
        return self.tablas

    def getDatos(self):
        return self.datos

    def setDatos(self,datos):
        self.datos = datos

    def getName(self):
        return self.name

    def delete(self,lista):
        '''elimina los elementos seleccionados del listview(lista)
            y los elimina del listview'''
        temp = []
        for i in range(len(self.getDatos())):
            if i not in lista:
                temp.append(self.getDatos()[i])
        self.setDatos(temp)



    def getArchivo(self):
        return self.path_archivo

    def myFirstPage(self,canvas,doc):
        '''configura la primera pagina del reporte
            como plantillas'''
        canvas.saveState()
        canvas.drawImage(self.logos,70,450,width=640,height=140)
        canvas.setFont('Times-Roman',10)
        canvas.drawImage(self.pie_con_firma,inch,40,width=640,height=65)
        canvas.drawString((self.PAGE_WIDTH/2.0)-20, 30, "Page %d" % (doc.page))
        canvas.restoreState()

    def myLaterPages(self,canvas, doc):
         '''configura las plantillas de las paginas secundarias'''
         canvas.saveState()

         canvas.drawImage(self.logos,70,450,width=640,height=140)

         canvas.setFont('Times-Roman',10)
         canvas.drawImage(self.pie_con_firma,inch,40,width=640,height=65)
         canvas.drawString((self.PAGE_WIDTH/2.0)-20, 30, "Page %d" % (doc.page))
         canvas.restoreState()

    def populate(self):
        # fecha = self.getFecha()
        fechaInicial=self.getFechaInicial()
        fechaFinal=self.getFechaFinal()
        dummies = open(self.path_archivo,'r').readlines()
        productos = []

        if fechaInicial !='' and fechaFinal !='':
            for n in dummies:
                if n[:8] >= fechaInicial and n[:8]<=fechaFinal:
                    if 'ï¿½' in n:
                        n = n.replace('ï¿½','ñ')
                    temp = n[12:-1]
                    comentario = temp[53:]
                    datos = temp[:53].split()
                    datos.append(comentario)
                    productos.append(datos)
        elif fechaInicial != "" and fechaFinal =="":
            for n in dummies:
                if n[:8] == fechaInicial:
                    if 'ï¿½' in n:
                        n = n.replace('ï¿½','ñ')
                    temp = n[12:-1]
                    comentario = temp[53:]
                    datos = temp[:53].split()
                    datos.append(comentario)
                    productos.append(datos)
        if productos:
            self.datos = productos


    def entablar(self,lista):
        '''# lista es la lista con todos los eventos del dia
        # maxx es es la maxima cantidad de eventos que se visualizaran una pagina
        # la funcion devuelve una lista de listas con maxx numero de elementos en
        # cada una de las listas'''
        # lista = self.getDatos()

        if lista == None or lista == []:
            return None
        entradas = []
        ent = []
        for i in range(1,len(lista)):
            ent.append(lista[i-1])
            if (i % self.maxx) ==0:
                entradas.append(ent)
                ent= []
        ent.append(lista[-1])
        entradas.append(ent)
        self.tablas = entradas

    def go(self,sentido=False):
        '''aqui se crea el pdf. si sentido = True entonces el reporte
            es para sismos sentidos, sino es para los eventos del dia'''
        nombre = ''
        space = 2
        tablas = self.tablas
        size = len(tablas)
        if tablas == None or tablas == []:
            return None

        if sentido == False:
            nombre = self.getName()
        else:
            nombre =  self.getName()[:-4]+' sismo sentido.pdf'

        doc = SimpleDocTemplate(nombre,pagesize=landscape(letter))
        Story = [Spacer(1,1.2*inch)]#dice que tan alto debe estar la parte dinamica del reporte
        for i in range(size):
                tablas[i].insert(0,self.cabecera2)
                tablas[i].insert(0,self.cabecera1)
                t = Table(tablas[i])
                t.setStyle([('SPAN',(2,0),(3,0)),
                            ('ALIGN',(2,0),(3,0),'CENTER'),
                            ('FONTSIZE',(0,0),(-1,-1),(10)),
                            ('ALIGN',(4,0),(4,-1),'RIGHT'),])
                Story.append(t)
                if i < size-1:
                    Story.append(Spacer(1,1.2*inch))

        doc.build(Story,onFirstPage=self.myFirstPage,onLaterPages=self.myLaterPages)
        os.system(nombre)


# fecha ="20190620"
# gr = GenReporte()
# gr.setFechaInicial(fecha)
# gr.crearNombrepdf()
# print(gr.getName())
# gr.populate()
# # gr.entablar()
# print(gr.getFecha())
# print(gr.getDatos())
# gr.delete(3)
# print(gr.getDatos())

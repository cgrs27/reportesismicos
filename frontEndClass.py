import tkinter as tk
import os
from tkinter import ttk
from tkinter import messagebox
from backendReporte import GenReporte

class Application(ttk.Frame):
    '''provee la interfaz entre el usuario y el generador
        re reportes GenReport'''

    def __init__(self,main_window):
        super().__init__(main_window)
        main_window.title("Ejemplo")
        # self.fecha_text =tk.StringVar()
        self.fechaInicial_text = tk.StringVar()
        self.fechaFinal_text = tk.StringVar()
        self.sb = ttk.Scrollbar(self)
        self.evento =""
        self.lista =[]


        self.listbox = tk.Listbox(self,yscrollcommand=self.sb.set,selectmode=tk.EXTENDED,height=35, width=130)
        self.listbox.grid(row=6,column=0,rowspan=6,columnspan=6)
        self.listbox.bind('<<ListboxSelect>>',self.getSelection)

        self.sb.grid(row=4,column=6,rowspan=8)

        self.sb.configure(command=self.listbox.yview)

        self.pack()
        # ###################################
        self.gr = GenReporte()
        self.ltitle = tk.Label(self,text='GenReport')
        self.ltitle.grid(row=0,column=3)

        self.lfechaInicial = tk.Label(self,text='Fecha Inicial')
        self.lfechaInicial.grid(row=2,column=0)

        self.lfechaFinal = tk.Label(self,text='Fecha Final')
        self.lfechaFinal.grid(row=3,column=0)

        self.lgrs = tk.Label(self,text='Generar reporte de sismos')
        self.lgrs.grid(row=2,column=4)
        #
        #
        self.lgrss = tk.Label(self,text='Generar reporte de sismo sentido')
        self.lgrss.grid(row=3,column=4)

        self.lcredito = tk.Label(self,text='Creado por Carlos Ramirez')
        self.lcredito.grid(row=17,column=4)


        # self.efecha = tk.Entry(self,textvariable=self.fecha_text)
        # self.efecha.grid(row=2,column=1)

        self.efechaInicial = tk.Entry(self,textvariable=self.fechaInicial_text)
        self.efechaInicial.grid(row=2,column=1)

        self.efechaFinal = tk.Entry(self,textvariable=self.fechaFinal_text)
        self.efechaFinal.grid(row=3,column=1)

        self.bsismos = tk.Button(self,text="Crear pdf", width=12, command=self.reporteSismos)
        self.bsismos.grid(row=2,column=5)

        self.bsentido = tk.Button(self,text="Enviar", width=12, command=self.enviar_sentido)
        self.bsentido.grid(row=3,column=5)
        # self.bsentido.config(state=tk.DISABLED)

        self.bclose = tk.Button(self,text="Close", width=12, command=self.salir)
        self.bclose.grid(row=4,column=5)

        self.bfecha = tk.Button(self,text="Mostrar", width=12, command=self.cargarLista)
        self.bfecha.grid(row=4,column=1)

        self.bdelete = tk.Button(self,text="Borrar Seleccion", width=12, command=self.getseleccion)
        self.bdelete.grid(row=17,column=0)


    def getseleccion(self):
        if self.lista != [] or len(self.lista)> 2:
            # print(self.lista[-1])
            # lista = list(self.lista[-1])

            self.listbox.delete(self.lista[0],self.lista[-1])
            self.gr.delete(self.lista)
            self.lista=[]

    def enviar_sentido(self):
        # print(self.lista,"OOOO")
        if self.lista == None or self.lista == []:
            mensage = "No hay datos"
            messagebox.showerror(title="No datos", message=mensage)
        else:
            path = self.gr.path_boletin
            if os.path.exists(path) == False or os.path.isdir(path)==False:
                os.mkdir(path)
            self.gr.set_sentidos(self.lista)
            lista = self.gr.sentidos
            self.gr.entablar(lista)
            self.gr.go(True)
        # self.gr.set_sentidos(self.lista)
        # lista = self.gr.sentidos
        # if lista != None and lista != []:
        #
        #     self.gr.entablar(lista)
        #     self.gr.go(True)
        # else:
        #     # print("no hay datos")
        #     mensage = "No hay datos"
        #     messagebox.showerror(title="No datos", message=mensage)

    def getSelection(self,event):
        '''crea una lista con los elementos seleccionados del listview'''

        # self.lista.append(self.listbox.curselection())
        self.lista = self.listbox.curselection()
        # print(self.listbox.curselection())

    def reporteSismos(self):
        if self.gr.getDatos() != None and self.gr.getDatos() != []:
            path = self.gr.path_boletin
            print(path)
            if os.path.exists(path) == False or os.path.isdir(path)==False:
                os.mkdir(path)
            lista = self.gr.getDatos()
            self.gr.entablar(lista)
            self.gr.go()
        else:
            # print("no hay datos")
            mensage = "No hay datos"
            messagebox.showerror(title="No datos", message=mensage)

    def cargarLista(self):
        self.listbox.delete(0,tk.END)
        self.gr.reset()
        self.lista =[]
        fi =self.fechaInicial_text.get()
        if fi == '' or fi == None:
            mensage = "Debe llenar la fecha inicial en formato aaaammdd"
            messagebox.showerror(title="llenar fecha inicial", message=mensage)
        if len(fi)<8 or len(fi)>8:
            mensage = "las fechas deben tener 8 digitos formato aaaammdd"
            messagebox.showerror(title="corregir fecha inicial", message=mensage)
        a = fi[:4]
        m = fi[5:7]
        d =fi[7:9]

        if self.gr.getFechaFinal() == '' or self.gr.getFechaFinal() != self.fechaFinal_text.get():
            self.gr.setFechaFinal(self.fechaFinal_text.get())
        if self.gr.getFechaInicial() == '' or self.gr.getFechaInicial() != self.fechaInicial_text.get():
            # self.listbox.delete(0,tk.END)
            self.gr.setFechaInicial(self.fechaInicial_text.get())
            self.gr.crearNombrepdf()
        self.gr.populate()

        for row in self.gr.getDatos():
            elem= ''
            for e in row:
                elem+=e+'  '
            self.listbox.insert(tk.END,elem)

    def callback():
        pass
    def salir(app):
        app.quit()

main_window = tk.Tk()
app = Application(main_window)
app.mainloop()
